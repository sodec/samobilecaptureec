//
//  PFDViewController.swift
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 27.05.2021.
//  Copyright © 2021 Sodec Bilisim Teknolojileri. All rights reserved.
//

import UIKit
import PDFKit
@objcMembers public class PFDViewController: UIViewController {

    @IBOutlet weak var pdfContainer: UIView!
    public var pdfUrl : URL?
    public override func viewDidLoad() {
        super.viewDidLoad()
        let bundle = Bundle(for: type(of: self))
            bundle.loadNibNamed("PFDViewController", owner: self, options: nil)
        // Do any additional setup after loading the view.
        
        if pdfUrl != nil {
                        let pdfView = PDFView(frame: self.pdfContainer.bounds)
                        self.pdfContainer.addSubview(pdfView)
                        pdfView.autoScales = true
            pdfView.document = PDFDocument(url: pdfUrl!)
        }
    }

    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
