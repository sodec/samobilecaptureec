/*
 * Copyright (C) 2018 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

#import "SADefineImage.h"

#define SA_MULTI_PAGE_FILE_NAME @"sa_multi"

@class SAMultiPageCamera;
@class SADetectDocument;

@protocol SAMultiPageCameraDelegate <NSObject>

@required
- (void)multiPageCameraDidCancel:(SAMultiPageCamera *)controller;
- (void)multiPageCameraDidClear:(SAMultiPageCamera *)controller;
- (void)multiPageCameraDidDone:(SAMultiPageCamera *)controller withSinglePagePath:(NSString *)singlePagePath withSinglePageCameraData:(NSData *)singlePageData withIsEncrypted:(BOOL)isEncrypted;

@end

@interface SAMultiPageCamera : UIViewController
{
    id<SAMultiPageCameraDelegate> __unsafe_unretained delegate;
}

@property (unsafe_unretained) id<SAMultiPageCameraDelegate> delegate;
@property (strong, nonatomic, readwrite) NSString *singlePageNavBarTitle;
@property (strong, nonatomic, readwrite) NSString *folderNameForSinglePage;
@property (strong, nonatomic, readwrite) NSString *folderNameForSingleDocument;
@property (strong, nonatomic, readwrite) NSString *directPath;
@property (strong, nonatomic, readwrite) NSString *captureDescription;
@property (strong, nonatomic, readwrite) NSString *captureNavBarTitle;
@property (strong, nonatomic, readwrite) NSString *processNavBarTitle;
@property (strong, nonatomic, readwrite) NSString *verifyNavBarTitle;
@property (nonatomic, assign) BOOL showProgress;
@property (nonatomic, assign) BOOL showIndicator;
@property (strong, nonatomic, readwrite) SADetectDocument *detectDocument;
@property (nonatomic, assign) BOOL blurDetection;
@property (nonatomic, assign) BOOL faceDetection;
@property (nonatomic, assign) SAImageEnhancing imageEnchancing;
@property (nonatomic, assign) BOOL encryptFile;

@end
