/*
 * Copyright (C) 2018 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

#import <SAMobileCapture/SALibrary.h>
#import <SAMobileCapture/SAConfig.h>
#import <SAMobileCapture/SAUIConfig.h>
#import <SAMobileCapture/SACropConfig.h>
#import <SAMobileCapture/SAScanConfig.h>
#import <SAMobileCapture/SAAlertConfig.h>
#import <SAMobileCapture/SAToastConfig.h>
#import <SAMobileCapture/SAIndicatorConfig.h>
#import <SAMobileCapture/SAProgressConfig.h>
#import <SAMobileCapture/SANotificationConfig.h>
#import <SAMobileCapture/SATipConfig.h>
#import <SAMobileCapture/SADefineImage.h>
#import <SAMobileCapture/SADefineDocument.h>
#import <SAMobileCapture/SADefineClassification.h>
#import <SAMobileCapture/SADetectDocument.h>
#import <SAMobileCapture/SAClassifyDocument.h>
#import <SAMobileCapture/SACaptureDocument.h>
#import <SAMobileCapture/SAProcessDocument.h>
#import <SAMobileCapture/SAVerifyDocument.h>
#import <SAMobileCapture/SAReadDocument.h>
#import <SAMobileCapture/SASinglePage.h>
#import <SAMobileCapture/SACropDocument.h>
#import <SAMobileCapture/SAFolioPage.h>
#import <SAMobileCapture/SAViewDocument.h>
#import <SAMobileCapture/SACrypto.h>
#import <SAMobileCapture/SAFileManager.h>
#import <SAMobileCapture/SASizeCalculator.h>
#import <SAMobileCapture/SACreatePDF.h>
#import <SAMobileCapture/SACreateFolio.h>
#import <SAMobileCapture/SAImageLoader.h>
#import <SAMobileCapture/SAUIUtility.h>
#import <SAMobileCapture/SAToast.h>
#import <SAMobileCapture/SAMultiPageCamera.h>
#import <SAMobileCapture/SCLAlertView.h>

