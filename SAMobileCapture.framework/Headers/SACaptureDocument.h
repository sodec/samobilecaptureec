/*
 * Copyright (C) 2018 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

#import "SADefineImage.h"
#import "SADefineDocument.h"
#import "SADefineClassification.h"

@class SACaptureDocument;
@class SADetectDocument;
@class SAClassifyDocument;

@protocol SACaptureDocumentDelegate <NSObject>

@required
- (void)captureDocumentDidCancel:(SACaptureDocument *)controller;
- (void)captureDocumentDidDone:(SACaptureDocument *)controller withDocumentPath:(NSString *)documentPath withIsEncrypted:(BOOL)isEncrypted withClassificationItem:(SAClassificationItem)classificationItem;

@end

@interface SACaptureDocument : UIViewController
{
    id<SACaptureDocumentDelegate> __unsafe_unretained delegate;
}

@property (unsafe_unretained) id<SACaptureDocumentDelegate> delegate;
@property (strong, nonatomic, readwrite) NSString *captureDescription;
@property (strong, nonatomic, readwrite) NSString *captureNavBarTitle;
@property (strong, nonatomic, readwrite) NSString *processNavBarTitle;
@property (strong, nonatomic, readwrite) NSString *verifyNavBarTitle;
@property (nonatomic, assign) BOOL showProgress;
@property (nonatomic, assign) BOOL showIndicator;
@property (strong, nonatomic, readwrite) SADetectDocument *detectDocument;
@property (nonatomic, assign) double blurScore;
@property (strong, nonatomic, readwrite) SAClassifyDocument *classifyDocument;
@property (nonatomic, assign) BOOL faceDetection;
@property (nonatomic, assign) SAImageQuality imageQaulity;
@property (nonatomic, assign) SADocumentType documentType;
@property (nonatomic, assign) SAImageEnhancing imageEnchancing;
@property (nonatomic, assign) SAImageFilter imageFilter;
@property (strong, nonatomic, readwrite) NSString *fileName;
@property (nonatomic, assign) SAImageFormat imageFormat;
@property (nonatomic, assign) CGFloat compressionQuality;
@property (nonatomic, assign) BOOL encryptFile;

@end
