//
//  SAMultiPagel.swift
//  Tesmer
//
//  Created by Erkan SIRIN on 25.04.2021.
//

import UIKit
import PDFKit


@objc public protocol SAMultiPageDelegate{
    func sAMultiPageDidDone(documentList:[String],pdfPath:URL)
}
    
    
@objcMembers public class SAMultiPage: UIViewController,SAMultiDocumentDelegate,SACropDocumentDelegate,SAMultiPageCameraDelegate {
  
    @IBAction func closePreviewView(_ sender: Any) {
        UIView.animate(withDuration: 0.4, animations: { [self] () -> Void in
            previewView.alpha = 0.0
        }, completion: { [self] (finished) -> Void in
            previewView.isHidden = true
        })
       
    }
    
    func didselectPreviewImage(filePath:String){
        DispatchQueue.main.async { [self] in
            previewView.isHidden = false
            let singlePageImage  = SAImageLoader.loadImage(fromDocumentPath: filePath, withIsEncrypted: isEncrypted)
            previewImageView.image = singlePageImage
            UIView.animate(withDuration: 0.4, animations: { [self] () -> Void in
                previewView.alpha = 1.0
            }, completion: { (finished) -> Void in
                
            })
            
            
            
        }
        
    }
    
    func didSelectionActive() {
        bottomEditBar.isHidden = true
        bottomBar.isHidden = false
        tumunuSecButton.isHidden = false
        
        cancelButton.isHidden = false
        backButton.isHidden = true
        
    }
    func didSelectionDone() {
        bottomEditBar.isHidden = false
        bottomBar.isHidden = true
        tumunuSecButton.isHidden = true
        
        cancelButton.isHidden = true
        backButton.isHidden = false
        
    }
    
    
    public func multiPageCameraDidCancel(_ controller: SAMultiPageCamera!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public func multiPageCameraDidClear(_ controller: SAMultiPageCamera!) {
        self.dismiss(animated: true) { [self] in
            setupFolderAndFiles()
        }
    }
    
    public func multiPageCameraDidDone(_ controller: SAMultiPageCamera!, withSinglePagePath singlePagePath: String!, withSinglePageCameraData singlePageData: Data!, withIsEncrypted isEncrypted: Bool) {
        
        
  
        self.dismiss(animated: true) { [self] in
            //let fileNameBoth = "\(Date().currentTimeMillis()).jpeg"
           // let url = URL(fileURLWithPath: singlePagePath)
            
//                    do {
//                        try singlePageImage?.jpegData(compressionQuality: 1.0)?.write(to: url)
//
//                    } catch {
//                        print(error)
//                        print("file cant not be save at path \(fileNameBoth), with error : \(error)");
//
//                    }
            if !multiDocView.documentPaths.contains(singlePagePath) {
                multiDocView.documentPaths.append(singlePagePath)
            }
            
            multiDocView.reloadData()
            //cropUrlList()
            setupFolderAndFiles()
        }
        
        
    }
    
    
    
    public func cropDocumentDidCancel(_ controller: SACropDocument!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public func cropDocumentDidDone(_ controller: SACropDocument!, withCroppedImage croppedImage: UIImage!) {
        
        self.dismiss(animated: true) { [self] in
            let fileNameBoth = "\(Date().currentTimeMillis()).jpeg"
            let url = URL(fileURLWithPath: NSTemporaryDirectory() + "\(fileNameBoth)")
            
                    do {
                        try croppedImage?.jpegData(compressionQuality: 1.0)?.write(to: url)
            
                    } catch {
                        print(error)
                        print("file cant not be save at path \(fileNameBoth), with error : \(error)");
            
                    }
            
           // multiDocView.documentPaths.append(url)
            multiDocView.reloadData()
            cropUrlList()
        }
               
        
        
    }
    
    func didselectDocument() {
        
    }
    
    func didTapDocument(indexPath: IndexPath, documentPats: [String]) {
        
//        let vc = PostProcessingDetailView()
//
//        vc.indexp = indexPath
//        vc.documentPaths = documentPats
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true, completion: nil)
        
        
        let multipageCamera = SAMultiPageCamera()
        multipageCamera.delegate = self
        multipageCamera.isEditing = true
        multipageCamera.encryptFile = isEncrypted
        multipageCamera.modalPresentationStyle = .fullScreen
        let urlpath = "\(documentPats[indexPath.row])"
        let fileNameArray = urlpath.split(separator: "/")
        let fileName = "\(fileNameArray.last ?? "")"
        print("fileName : ",fileName)
        print("fileNameArray : ", String(fileName.dropLast(6)))
        multipageCamera.folderNameForSinglePage = folderNameForMultiPage
        multipageCamera.folderNameForSingleDocument = String(fileName.dropLast(6))
        
        self.present(multipageCamera, animated: true, completion: nil)
    }
    
    func dragDropCollectionViewDidMoveCellFromInitialIndexPath(_ initialIndexPath: IndexPath, toNewIndexPath newIndexPath: IndexPath) {
        
    }
    
    var tempUrlList :[URL] = []
    func cropUrlList(){
        if tempUrlList.count > 0 {
            
            if let imageData = try? Data(contentsOf: tempUrlList[0]) {
                 
                
                let cropDocument = SACropDocument()
                cropDocument.delegate = self
                cropDocument.sourceImage = UIImage(data: imageData)
                tempUrlList.remove(at: 0)
                cropDocument.modalPresentationStyle = .fullScreen
                self.present(cropDocument, animated: true, completion: nil)
            }
            
            
            
        }
    }
    func didSelectGalleryItems(urlList: [URL]) {
        print("urlList : ",urlList)
        tempUrlList = urlList
        
        cropUrlList()
        
        
    }
    
    
    public var delegate : SAMultiPageDelegate?
    
    @IBOutlet weak var importButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var bottomEditBar: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var tumunuSecButton: UIButton!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var multiDocView: SAMultiDocument!
    @objc public var folderNameForMultiPage = SA_MULTI_PAGE_FILE_NAME
    @objc public var isEncrypted : Bool = false
    @objc public func setIsEncrypted(encrypted:Bool=false){
        isEncrypted = encrypted
    }
    @objc public var captureNavBarTitle = "Fatura Ekle"
    @objc public func setParam(captureNavBarTitle:NSString){
        self.captureNavBarTitle = "\(captureNavBarTitle)"
    }
    
    @objc public var captureDescription = "Lütfen elektrik faturanızı ekleyiniz."
    @objc public func setParam(captureDescription:NSString){
        self.captureDescription = "\(captureDescription)"
    }
    
    @objc public var singlePageNavBarTitle = "Elektrik Faturası"
    @objc public func setParam(singlePageNavBarTitle:NSString){
        self.singlePageNavBarTitle = "\(singlePageNavBarTitle)"
    }
    
    @objc public var processNavBarTitle = "Düzenle"
    @objc public func setParam(processNavBarTitle:NSString){
        self.processNavBarTitle = "\(processNavBarTitle)"
    }
    
    @objc public var verifyNavBarTitle = "Onayla"
    @objc public func setParam(verifyNavBarTitle:NSString){
        self.verifyNavBarTitle = "\(verifyNavBarTitle)"
    }
    
    @objc public var multiPageTitle = "Doküman Editörü"
    @objc public func setParam(multiPageTitle:NSString){
        self.multiPageTitle = "\(multiPageTitle)"
    }
    
    @objc public var showProgress : Bool = false
    @objc public func setShowProgress(showProgress:Bool=false){
        self.showProgress = showProgress
    }
    
    @objc public var showIndicator : Bool = false
    @objc public func setShowIndicator(showIndicator:Bool=false){
        self.showIndicator = showIndicator
    }
    
    @objc public var blurDetection : Bool = false
    @objc public func setBlurDetection(blurDetection:Bool=false){
        self.blurDetection = blurDetection
    }
    
    @objc public var faceDetection : Bool = false
    @objc public func setFaceDetection(faceDetection:Bool=false){
        self.faceDetection = faceDetection
    }
    
    @objc public var imageEnchancing : SAImageEnhancing = .autoColor
    
  
    @objc public var detectDocumentEnabled : Bool = false
    @objc public func setDetectDocumentEnabled(detectDocumentEnabled:Bool=false){
        self.detectDocumentEnabled = detectDocumentEnabled
    }
    
    @objc public var minCountOfLetters : Int32 = 50
    @objc public func setMinCountOfLetters(minCountOfLetters : Int32 = 50){
        self.minCountOfLetters = minCountOfLetters
    }
    
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let bundle = Bundle(for: type(of: self))
            bundle.loadNibNamed("SAMultiPage", owner: self, options: nil)
        
        let uiConfig = SAUIConfig()
        
        self.view.backgroundColor = uiConfig.navBarColor
        
        titleLabel.text = multiPageTitle
        bottomBar.isHidden = true
        tumunuSecButton.isHidden = true
        cancelButton.isHidden = true
        previewView.isHidden = true
        previewView.alpha = 0.0
        setupFolderAndFiles()
        multiDocView.draggingDelegate = self
        multiDocView.isEncrypted = isEncrypted
        multiDocView.setupCollectionView()
        
        
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        if multiDocView.documentPaths.count <= 0 {
            addNewItem()
        }
    }
    func setupFolderAndFiles() {

        multiDocView.documentPaths.removeAll()
       let fileManager = FileManager.default
  

       let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
       let pathComponent = documentsURL.appendingPathComponent("\(folderNameForMultiPage)")
       do {
           let fileURLs = try fileManager.contentsOfDirectory(at: pathComponent, includingPropertiesForKeys: nil)
          
           for file in fileURLs {
            let filePath = "\(file.absoluteString)"
            multiDocView.documentPaths.append("\(filePath.dropFirst(8))")
           
           }
     
        
       } catch {
    
           print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
       }
       print("multiDocView.documentPaths : ",multiDocView.documentPaths)
        multiDocView.reloadData()
       
       // }
       
       
   }
    
    @IBAction func tumunuSecButtonAction(_ sender: Any) {
        multiDocView.selectAllItems()
        
    }
    @IBAction func editButtonAction(_ sender: Any) {
        bottomEditBar.isHidden = true
        bottomBar.isHidden = false
        tumunuSecButton.isHidden = false
        cancelButton.isHidden = false
        backButton.isHidden = true
        multiDocView.selectionIsActive = true
        multiDocView.reloadData()
    }
    @IBAction func saveButtonAction(_ sender: Any) {
        createPDFDocument()
    }
    
    func createPDFDocument(){
        
        let pdfDocument = PDFDocument()
        
        for url in multiDocView.documentPaths {
            
            
            let image  = SAImageLoader.loadImage(fromDocumentPath: url, withIsEncrypted: isEncrypted)
            let pdfPage = PDFPage(image: image!)
            pdfDocument.insert(pdfPage!, at: pdfDocument.pageCount)
                
         
            
        }
        
        let data = pdfDocument.dataRepresentation()
        let fileNameBoth = "\(Date().currentTimeMillis()).pdf"
        let pdfurl = URL(fileURLWithPath: NSTemporaryDirectory() + "\(fileNameBoth)")
        do {
            try data!.write(to: pdfurl)

        } catch {
            print(error)
            print("pdf file cant not be save at path \(pdfurl), with error : \(error)");

        }
        
        print("pft path : ",pdfurl);
        
        
        delegate?.sAMultiPageDidDone(documentList: multiDocView.documentPaths, pdfPath: pdfurl)
        
//        DispatchQueue.main.async {
//            let pdfView = PDFView(frame: self.view.bounds)
//            self.view.addSubview(pdfView)
//            pdfView.autoScales = true
//            pdfView.document = PDFDocument(url: pdfurl)
//        }
                
        
    }
    @IBAction func cancelButtonAction(_ sender: Any) {
        bottomEditBar.isHidden = false
        bottomBar.isHidden = true
        tumunuSecButton.isHidden = true
        
        cancelButton.isHidden = true
        backButton.isHidden = false
        
        multiDocView.selectionIsActive = false
        multiDocView.selectedItems.removeAll()
        multiDocView.reloadData()
    }
    @IBAction func importButtonAction(_ sender: Any) {
//        let vc = PostProcessingView()
//        vc.delegate = self
//        //vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true, completion: nil)
        
    }
    
    var itemsToRemove : [String] = []
    @IBAction func deleteButtonAction(_ sender: Any) {
  
        let alertConfig = SAAlertConfig()
        let uiConfig = SAUIConfig()
        

        let positiveButtonBuilder = SCLALertViewButtonBuilder()

        positiveButtonBuilder.title("Sil")
        positiveButtonBuilder.actionBlock ({
            
            DispatchQueue.main.async {
                [self] in
                for i in multiDocView.selectedItems {
             
                    
                    let url = multiDocView.documentPaths[i]
                    itemsToRemove.append(url)
                   
                }
                
                deleteItmes()
            }
            
                        })
           
                    let negativeButtonBuilder = SCLALertViewButtonBuilder()
                    negativeButtonBuilder.title("İptal")
                    negativeButtonBuilder.actionBlock({
        
                    });
        
        
                    let builder = SCLAlertViewBuilder()
        builder?.backgroundType(.shadow)
        builder?.shouldDismissOnTapOutside(false);
                    builder?.useLargerIcon(false);
        builder?.iconTintColor(.white);
        
        
        
                    builder?.addButtonWithBuilder(positiveButtonBuilder);
                    builder?.addButtonWithBuilder(negativeButtonBuilder);
        
                    let showBuilder = SCLAlertViewShowBuilder()
        
        if (alertConfig.alertViewWarningImage != nil && alertConfig.alertViewWarningImage.count > 0)
                    {
            
            showBuilder.style(.custom);
                        showBuilder.image(UIImage(named: alertConfig.alertViewWarningImage));
                        showBuilder.color(uiConfig.warningColor);
                    }
                    else
                    {
                        showBuilder.style(.warning);
                    }
        
                    showBuilder.title("Onayla");
                    showBuilder.subTitle("Seçtiğiniz evraklar silinecektir. Devam etmek istiyor musunuz?")
                    showBuilder.duration(0.0);
        
        showBuilder.show(builder!.alertView, on: self)
              
               
        
        
     
        

//            positiveButtonBuilder.actionBlock(^{
//                NSString *dataPath = nil;
//
//                if (self.encryptFile)
//                {
//                    dataPath = [NSString stringWithFormat:@"/%@/%@.sodec", self.folderNameForSinglePage, self.folderNameForSingleDocument];
//                }
//                else
//                {
//                    dataPath = [NSString stringWithFormat:@"/%@/%@.jpeg", self.folderNameForSinglePage, self.folderNameForSingleDocument];
//
//                    //dataPath = folderPath;
//
//
//                }
//
//
//                //[SAFileManager removeFile:dataPath];
//
//                NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
//                NSFileManager *fileManager = [[NSFileManager alloc] init];
//
//
//
//                NSError *fileError;
//
//                        NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, dataPath];
//                        [fileManager removeItemAtPath:filePath error:&fileError];
//
//
//
//
//                [self.delegate multiPageCameraDidClear:self];
//            });
//
//            SCLALertViewButtonBuilder *negativeButtonBuilder = [SCLALertViewButtonBuilder new];
//            negativeButtonBuilder.title(NSLocalizedString(@"İptal", @"Show it in the alert view as a button"));
//            negativeButtonBuilder.actionBlock(^{
//
//            });
//
//            SCLAlertViewBuilder *builder = [SCLAlertViewBuilder new];
//            builder.backgroundType(SCLAlertViewBackgroundShadow);
//            builder.shouldDismissOnTapOutside(NO);
//            builder.useLargerIcon(NO);
//            builder.iconTintColor([UIColor whiteColor]);
//
//            @try
//            {
//                builder.setTitleFontFamily(alertConfig.alertViewTitleFontFamily, alertConfig.alertViewTitleFontSize);
//            }
//            @catch (NSException *exception)
//            {
//                builder.setTitleFontFamily(@"TrebuchetMS-Bold", alertConfig.alertViewTitleFontSize);
//            }
//
//            @try
//            {
//                builder.setBodyTextFontFamily(alertConfig.alertViewBodyFontFamily, alertConfig.alertViewBodyFontSize);
//            }
//            @catch (NSException *exception)
//            {
//                builder.setBodyTextFontFamily(@"TrebuchetMS", alertConfig.alertViewBodyFontSize);
//            }
//
//            @try
//            {
//                builder.setButtonsTextFontFamily(alertConfig.alertViewButtonsFontFamily, alertConfig.alertViewButtonsFontSize);
//            }
//            @catch (NSException *exception)
//            {
//                builder.setButtonsTextFontFamily(@"TrebuchetMS-Bold", alertConfig.alertViewButtonsFontSize);
//            }
//
//            builder.addButtonWithBuilder(positiveButtonBuilder);
//            builder.addButtonWithBuilder(negativeButtonBuilder);
//
//            SCLAlertViewShowBuilder *showBuilder = [SCLAlertViewShowBuilder new];
//
//            if (alertConfig.alertViewWarningImage != nil && [alertConfig.alertViewWarningImage length] > 0)
//            {
//                showBuilder.style(SCLAlertViewStyleCustom);
//                showBuilder.image([UIImage imageNamed:alertConfig.alertViewWarningImage]);
//                showBuilder.color(uiConfig.warningColor);
//            }
//            else
//            {
//                showBuilder.style(SCLAlertViewStyleWarning);
//            }
//
//            showBuilder.title(NSLocalizedString(@"Onayla", @"Use it as the title in the warning alert view to delete or save the document"));
//            showBuilder.subTitle(NSLocalizedString(@"Eklediğiniz evrak silinecektir. Devam etmek istiyor musunuz?", @"Use it as the body in the warning alert view to delete the document"));
//            showBuilder.duration(0.0f);
//
//            [showBuilder showAlertView:builder.alertView onViewController:self];
//        }
//        else
//        {
//            [self showToast:NSLocalizedString(@"Henüz evrak eklemediniz", @"Use it as the body in the toast if any document is not captured")];
//        }
//
    
    
        
//        let alert = UIAlertController(title: "BİLGİ!", message: "Eklediğiniz evrak silinecektir. Devam etmek istiyor musunuz?",         preferredStyle: UIAlertController.Style.alert)
//
//        alert.addAction(UIAlertAction(title: "Vazgeç", style: UIAlertAction.Style.default, handler: { _ in
//            //Cancel Action
//        }))
//        alert.addAction(UIAlertAction(title: "Sil",
//                                      style: UIAlertAction.Style.default,
//                                      handler: {(_: UIAlertAction!) in
//                                        DispatchQueue.main.async {
//                                            [self] in
//                                            for i in multiDocView.selectedItems {
//
//
//                                                let url = multiDocView.documentPaths[i]
//                                                itemsToRemove.append(url)
//
//                                            }
//
//                                            deleteItmes()
//                                        }
//
//
//
//                                      }))
//        self.present(alert, animated: true, completion: nil)
        
        
        
        
        
       
        
    }
    func deleteItmes(){
        for item in itemsToRemove {
     
            if let index = multiDocView.documentPaths.firstIndex(of: item) {
                deleteFile(file: item)
                multiDocView.documentPaths.remove(at: index)
            }
            
        
        }
        
        
        multiDocView.selectedItems.removeAll()
        multiDocView.selectionIsActive = false
        bottomEditBar.isHidden = false
        bottomBar.isHidden = true
        tumunuSecButton.isHidden = true
        
        cancelButton.isHidden = true
        backButton.isHidden = false
        
        multiDocView.reloadData()
    }
    
    fileprivate func deleteFile(file: String) {
   
        let url = URL(fileURLWithPath: file)
     
        
        do {
            try FileManager.default.removeItem(at: url)
            print ("file deleted \(url.lastPathComponent)")
        } catch {
            print("Could not remove file at url: \(url.lastPathComponent)")
        }
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        addNewItem()
    }
    
    
    
    
    
    func addNewItem(){
        //SAFileManager.removeFile(SA_SINGLE_PAGE_FILE_NAME)
        
        let fileNameBoth = "\(Date().currentTimeMillis())"
        //let url =  NSTemporaryDirectory() + "\(fileNameBoth)"
        
//        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
//        let url = NSURL(fileURLWithPath: path)
//        let pathComponent = url.appendingPathComponent("\(file)")
//
        
        
        let multipageCamera = SAMultiPageCamera()
        multipageCamera.delegate = self
        multipageCamera.folderNameForSinglePage = folderNameForMultiPage
        multipageCamera.folderNameForSingleDocument = fileNameBoth
        multipageCamera.encryptFile = true
        multipageCamera.singlePageNavBarTitle = captureNavBarTitle
        multipageCamera.captureNavBarTitle = singlePageNavBarTitle
        multipageCamera.captureDescription = captureDescription
        multipageCamera.processNavBarTitle = processNavBarTitle
        multipageCamera.showProgress = showProgress
        multipageCamera.blurDetection = blurDetection
        multipageCamera.faceDetection = faceDetection
        multipageCamera.imageEnchancing = imageEnchancing
        
        let saDetectDocument = SADetectDocument()
        saDetectDocument.enabled = detectDocumentEnabled
        saDetectDocument.minCountOfLetters = minCountOfLetters
        multipageCamera.detectDocument = saDetectDocument
        multipageCamera.modalPresentationStyle = .fullScreen
        self.present(multipageCamera, animated: true, completion: nil)
        
     

        
        
    }
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

struct UIDecorators {
      
    static let darkish_blue = UIColor(red: 0/255, green: 61/255, blue: 166/255, alpha: 1.0)
    
    static let colorBackground = UIColor(red: 57/255, green: 66/255, blue: 100/255, alpha: 1.0)
    static let colorPred = UIColor(red: 230/255, green: 76/255, blue: 101/255, alpha: 1.0)
    static let colorLiteBlue = UIColor(red: 26/255, green: 79/255, blue: 149/255, alpha: 1.0)
    static let colorBook = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0)
    static let darkGreen = UIColor(red: 74/255, green: 181/255, blue: 39/255, alpha: 1.0)
    
    static let ProjectFontBold=UIFont(name: "OpenSans-Bold", size: 14)
    static let ProjectFont=UIFont(name: "OpenSans", size: 14)
    static let ProjectFontSemiBold=UIFont(name: "OpenSans-SemiBold", size: 12)
    
    static let viewBorderWidth: CGFloat = 2
    static let UIViewRadious: CGFloat = 18
    static let UIButtonRadious: CGFloat = 5
    static let screenSize = UIScreen.main.bounds
    static let buttonRadious: CGFloat = 15
    static let buttonSmallRadious: CGFloat = 10
    static let bigButtonRadious: CGFloat = 25
    static let notificationButtonRadious: CGFloat = 5
    static let userFaceButtonRadious: CGFloat = 37
    static let mapViewHight: CGFloat = 455
    static let buttonBorder: CGFloat = 15
    static let formCellHeight : CGFloat = 60
    static let headerAnimationSpeed : TimeInterval = 1.0
    
    static let colorPastelRed1 = UIColor(red: 204/255, green: 50/255, blue: 75/255, alpha: 1.0)
    static let colorCodeOrange = UIColor(red: 255/255, green: 180/255, blue: 0/255, alpha: 1.0)
    static let colorCodeGray = UIColor(red: 103/255, green: 103/255, blue: 103/255, alpha: 1.0)
    static let colorCodeDarkGray = UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1.0)
    static let colorCodeLightGray = UIColor(red: 161/255, green: 161/255, blue: 161/255, alpha: 1.0)
    static let colorCodeLightGrayWithOpacty = UIColor(red: 226/255, green: 220/255, blue: 217/255, alpha: 1.0)
    static let colorCodeLightGrayWithOpacty2 = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1.0)
    static let colorCodeUltraLightGray = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0)
    static let colorCodeUltraVeryLightGray = UIColor(red: 221/255, green: 221/255, blue: 221/255, alpha: 1.0)
    static let colorCodeWhite = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
    static let colorCodeBlue = UIColor(red: 46/255, green: 216/255, blue: 255/255, alpha: 1.0)
    static let borderColor = UIColor(red: 188/255, green: 187/255, blue: 187/255, alpha: 1.0)
    static let colorCodeTurkuazBlue = UIColor(red: 28/255, green: 192/255, blue: 230/255, alpha: 1.0)
    static let colorCodeRed = UIColor(red: 255/255, green: 57/255, blue: 57/255, alpha: 1.0)
    static let colorCodeLightBrown = UIColor(red: 231/255, green: 222/255, blue: 217/255, alpha: 1.0)
    static let colorCodeCreamy = UIColor(red: 241/255, green: 235/255, blue: 231/255, alpha: 1.0)
    static let twilightBlue = UIColor(red: 13/255, green: 75/255, blue: 160/255, alpha: 1.0)
    static let instalmentPriceGray = UIColor(red: 99/255, green: 99/255, blue: 99/255, alpha: 1.0)
    static let instalmentTitleGray = UIColor(red: 127/255, green: 127/255, blue: 127/255, alpha: 1.0)
    static let whiteThirteen = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0)
    static let colorCodeClear = UIColor.clear
    
    static var loadingImages = [UIImage]()
    
}

extension UIView {
    @IBInspectable var SACornerRadius: CGFloat {
        get {
            return layer.cornerRadius+UIDecorators.UIViewRadious
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    
    @IBInspectable var SACornerWidth: CGFloat {
        get {
            return UIDecorators.UIViewRadious
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable
    var SAborderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var SAborderColor: UIColor? {
        get {
            let color = UIColor(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var SAshadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var SAshadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var SAshadowColor: UIColor? {
        get {
            return UIColor(cgColor: layer.shadowColor!)
        }
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var SAshadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    

    
      /*
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = shadowOffset
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = shadowOpacity
            
        }
    }
    
  
    
    
    
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            let color = UIColor(cgColor: layer.shadowColor!)
            return color
        }
        set {
            layer.shadowColor =  UIColor.black.cgColor
        }
    }*/
}

extension Date {
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
