/*
 * Copyright (C) 2018 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

@class SAReadDocument;

@protocol SAReadDocumentDelegate <NSObject>

@required
- (void)readDocumentDidCancel:(SAReadDocument *)controller;
- (void)readDocumentDidReceiveMemoryWarning:(SAReadDocument *)controller withElapsedTime:(int)seconds;
- (void)readDocumentDidTimeout:(SAReadDocument *)controller;
- (void)readDocumentDidDone:(SAReadDocument *)controller withFoundedText:(NSString *)foundedText withElapsedTime:(int)seconds;

@end

typedef NS_ENUM(NSInteger, SACaptureQuality)
{
    SACaptureQualityDefault,
    SACaptureQualityMedium,
    SACaptureQualityHigh
};

typedef NS_ENUM(NSInteger, SAOCRAccuracyLevel)
{
    SAOCRAccuracyLevelDefault,
    SAOCRAccuracyLevelHigh,
    SAOCRAccuracyLevelMedium
};

@interface SAReadDocument : UIViewController
{
    id<SAReadDocumentDelegate> __unsafe_unretained delegate;
}

@property (unsafe_unretained) id<SAReadDocumentDelegate> delegate;
@property (strong, nonatomic, readwrite) NSString *readDocumentNavBarTitle;
@property (nonatomic, assign) SACaptureQuality captureQaulity;
@property (nonatomic, assign) SAOCRAccuracyLevel ocrAccuracyLevel;
@property (nonatomic, assign) BOOL activateRectangleCrop;
@property (nonatomic, assign) NSTimeInterval timeout;

@end
